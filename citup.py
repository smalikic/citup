# Created by Salem Malikic on May 20 2019

#!/usr/bin/env python
from gurobipy import *
import numpy as np
from datetime import datetime
import argparse
import os
import errno

epsilon = 0.0000000001

def roundInt(x):
	return int(x+0.5)



def isFloat(value):
	try:
		float(value)
		return True
	except ValueError:
    		return False


def floatToStr(floatNumber, numDecimals = 4):
	if isFloat(floatNumber):
		return ("{:." + str(numDecimals) + "f}").format(floatNumber)
	else:
		return floatNumber


class Edge:
	def __init__(self, parentNode, childNode):
		self.parentNode = parentNode
		self.childNode  = childNode



def getNumberOfDistinctTrees(pathAncestryMatrixFile):
	assert os.path.exists(pathAncestryMatrixFile), "ERROR in function getNumberOfDistinctTrees. There does not exist file " + pathAncestryMatrixFile
	ancestryMatrixFile = open(pathAncestryMatrixFile, "r")
	numTrees = int(ancestryMatrixFile.readline().rstrip().split()[1])
	ancestryMatrixFile.close()
	return numTrees
		 


# Ancestry and gamma matrix are synonyms
# Tree indexing is 0-based
def readAncestryMatrix(pathAncestryMatrixFile, treeSize, treeIndex):
	numTrees = getNumberOfDistinctTrees(pathAncestryMatrixFile)
	assert treeIndex < numTrees, "ERROR. Trying to access tree with index " + str(treeIndex) + " in " + pathAncestryMatrixFile  + ". Too large index (0-based indexing is used)"

	ancestryMatrixFile = open(pathAncestryMatrixFile, "r")

	numLinesToSkip = 1 + treeIndex * (treeSize+1) 
	for i in range(numLinesToSkip): # skipping lines before the lines encoding tree of interest
		ancestryMatrixFile.readline()

	A = []
	for i in range(treeSize):
		line = ancestryMatrixFile.readline()
		lineColumns = line.strip().split()
		assert len(lineColumns) == treeSize, "ERROR in readAncestryMatrix. Number of numbers in line different from tree size. Line is: " + l
		A.append([int(lineColumns[j]) for j in range(treeSize)])
		
	ancestryMatrixFile.close()

	return A



def readTreeEdges(pathAdjacencyMatrixFile, treeSize, treeIndex):
	assert os.path.exists(pathAdjacencyMatrixFile), "ERROR in readTreeEdges function. File " + pathAdjacencyMatrixFile + " does not exist."
	adjacencyMatrixFile = open(pathAdjacencyMatrixFile, "r")
	
	numLinesToSkip = 2 + treeIndex
	for i in range(numLinesToSkip):
		adjacencyMatrixFile.readline() 

	edges = []
	line = adjacencyMatrixFile.readline()
	lineColumns = line.strip().split()
	assert (int(lineColumns[0]) == treeSize - 1) and (len(lineColumns) == 2*treeSize-1),  "ERROR in readTreeEdges in line " + line + ". Discordant number of edges and tree size."
	edges = []
	i = 1
	while i < len(lineColumns):
		edges.append(Edge(int(lineColumns[i]), int(lineColumns[i+1])))
		i += 2
	
	adjacencyMatrixFile.close()
	
	return edges	
		

def getRootDegree(pathAdjacencyMatrixFile, treeSize, treeIndex):
	E = readTreeEdges(pathAdjacencyMatrixFile, treeSize, treeIndex)
	rootDegree = 0
	for e in E:
		if e.parentNode == 0:
			rootDegree += 1
	return rootDegree




# ======== COMMAND LINE THINGS
parser = argparse.ArgumentParser(description='big_brother', add_help=True)
# Required:
parser.add_argument('-f', '--frequenciesFile', required=True,
                    type=str,
                    help='Input frequencies file')

parser.add_argument('-g', '--gammaMatricesFolder', required=True,
                    type=str,
                    help='folder containing matrices (in the CITUP release it is named: GammaAdjMatrices)')

parser.add_argument('-o', '--pathOutputFilePrefix', required=True, type=str, help = 'Prefix of the output file')

args = parser.parse_args()


'''
An example of input file is:

mutID	sampe0	sample1	clusterID
M1	0.4	0.3	1
M2	0.2	0.5	2
M3	0.44	0.28	1
M4	0.42	0.33	1
M5	0.22	0.772	ghostCluster

Note that mutID and clusterID are arbitrary strings (i.e. clusterIDs do not have to be numbers). 
MutIDs must not repeat. 
File is expected to be tab separated.
'''

f = {}  # f[mutID] = {},  f[mutID][sampleID] = VAF value
c = {}  # c[mutID] = clusterID
mutIDs	   = []
clusterIDs = []

frequenciesFile = open(args.frequenciesFile, "r")
headerLine  = frequenciesFile.readline().strip().split()
sampleIDs   = headerLine[1:(len(headerLine)-1)]
numSamples  = len(sampleIDs)

for line in frequenciesFile:
	lineColumns   =  line.strip().split()

	ERROR_MESSAGE =  "ERROR in input file. Some of the possible reasons are:"
	ERROR_MESSAGE += "\n(i)   mutID or clusterID missing or maybe they have empty spaces"
	ERROR_MESSAGE += "\n(ii)  number of samples different from the one in header"
	ERROR_MESSAGE += "\n(iii) separator used between columns is not tab or empty space."
	ERROR_MESSAGE += "\n----- Error is in the following line: " + line
	assert len(lineColumns) - 2 == numSamples, ERROR_MESSAGE

	mutID = lineColumns[0]
	assert mutID not in mutIDs, "ERROR. Mutation ID (first column) " + mutID + " appears more than once. These IDs must be unique"
	mutIDs.append(mutID)
	f[mutID] = {}
	for i in range(numSamples):
		f[mutID][sampleIDs[i]] = float(lineColumns[i+1])
	
	clusterID = lineColumns[-1]
	if clusterID not in clusterIDs:
		clusterIDs.append(clusterID)
	c[mutID] = clusterID
		
frequenciesFile.close()



numMutations = len(f)
numClusters  = len(clusterIDs)
treeSize =  numClusters + 1
assert 2 <= treeSize <= 10, "ERROR. This implementation supports up to 9 clusters. Currently there are " + str(numClusters) + " clusters."
pathAncestryMatrixFile  = args.gammaMatricesFolder.rstrip(os.sep) + os.sep + "GammaMatrix" + str(treeSize) + ".txt"
pathAdjacencyMatrixFile = args.gammaMatricesFolder.rstrip(os.sep) + os.sep + "AdjacencyMatrix" + str(treeSize) + ".txt"
numTrees = getNumberOfDistinctTrees(pathAncestryMatrixFile)



treeScoresFile = open(args.pathOutputFilePrefix + ".treeScores.tsv", "w")
treeScoresFile.write("treeIndex\ttreeScore\trootDegree\n")

for treeIndex in range(numTrees):
	A = readAncestryMatrix(pathAncestryMatrixFile, treeSize, treeIndex)
	E = readTreeEdges(pathAdjacencyMatrixFile, treeSize, treeIndex)
	model = Model('CITUP_MQIP')
	#model.Params.Threads = 1
	#model.Params.LogFile = ""

	print('Building model variables...')


	delta = {}
	for clusterID in clusterIDs:
		for v in range(treeSize):
			delta[clusterID, v] = model.addVar(vtype=GRB.BINARY, name='delta[{0},{1}]'.format(clusterID, v))
	
	for clusterID in clusterIDs:
		model.addConstr(quicksum(delta[clusterID, v] for v in range(0, treeSize)) == 1)
	
	for v in range(treeSize):
		if v == 0: # root node is mutation free
			for clusterID in clusterIDs:
				model.addConstr(delta[clusterID, v] == 0)
		else:
			model.addConstr(quicksum(delta[clusterID, v] for clusterID in clusterIDs) == 1)
	


	x = {} # frequencies of individual nodes
	for v in range(treeSize):
		for s in range(numSamples):
			sampleID = sampleIDs[s]
			x[v, sampleID] = model.addVar(vtype=GRB.CONTINUOUS, name='x[{0},{1}]'.format(v,sampleID), lb=0, ub=1)
	
	for s in range(numSamples):
		sampleID = sampleIDs[s]
		model.addConstr(quicksum(x[v, sampleID] for v in range(treeSize)) == 1)


	y = {} # lineage frequencies
	for v in range(treeSize):
		for s in range(numSamples):
			sampleID = sampleIDs[s]
			y[v, sampleID] = model.addVar(vtype=GRB.CONTINUOUS, name='y[{0},{1}]'.format(v,sampleID))
			model.addConstr(y[v, sampleID] == quicksum(A[v][u]*x[u, sampleID] for u in range(treeSize)))
	
	objective = 0
	z = {}		
	for i in range(numMutations):
		for v in range(treeSize):
			for s in range(numSamples):
				mutID     = mutIDs[i]
				clusterID = c[mutID]
				sampleID  = sampleIDs[s]
				z[mutID, v, sampleID] = model.addVar(vtype=GRB.CONTINUOUS, name='z[{0},{1},{2}]'.format(mutID, v, sampleID), lb=0)
				model.addConstr(z[mutID, v, sampleID] >= delta[clusterID, v] - 1 + (f[mutID][sampleID] - y[v, sampleID]))
				model.addConstr(z[mutID, v, sampleID] >= delta[clusterID, v] - 1 - (f[mutID][sampleID] - y[v, sampleID]))
				objective += z[mutID, v, sampleID] * z[mutID, v, sampleID]
	
	model.setObjective(objective, GRB.MINIMIZE)
	
	model.optimize()
	
	nodeOfCluster = {}
	clusterOfNode = {}
	clusterOfNode[0] = "healthy"
	nodeOfCluster["healthy"] = 0
	for clusterID in clusterIDs:
		for v in range(treeSize):
			if roundInt(delta[clusterID, v].X) == 1:
				assert clusterID not in list(nodeOfCluster.keys()), "ERROR. Cluster " + clusterID + " assigned to multiple nodes" 
				nodeOfCluster[clusterID] = v
				assert v not in list(clusterOfNode.keys()), "ERROR. Node " + str(v) + " has multiple clusters assigned to it"
				clusterOfNode[v] = clusterID



	treeScoresFile.write(str(treeIndex) + "\t" + str(objective.getValue()) + "\t" + str(getRootDegree(pathAdjacencyMatrixFile, treeSize, treeIndex)) + "\n")
	outputTreeFile = open(args.pathOutputFilePrefix + ".tree_" + str(treeIndex) + ".txt", "w")
	outputTreeFile.write("TREE_SCORE" + "\n")
	outputTreeFile.write(str(objective.getValue()) + "\n")
	outputTreeFile.write("\n")

	outputTreeFile.write("PARENT_NODE\tCHILD_NODE" + "\n")
	for e in E:
		parentNode = e.parentNode
		childNode  = e.childNode
		outputTreeFile.write(str(parentNode) + "\t" + str(childNode) + "\n")
	outputTreeFile.write("\n")

	
	outputTreeFile.write("CLUSTER\tNODE_OF_CLUSTER\n")
	for clusterID in clusterIDs:
		outputTreeFile.write(str(clusterID) + "\t" + str(nodeOfCluster[clusterID]) + "\n")
	outputTreeFile.write("\n")
	

	outputTreeFile.write("MUTATION\tNODE_OF_MUTATION\n")
	for mut_id in mutIDs:
		v = nodeOfCluster[c[mut_id]]
		outputTreeFile.write(mut_id + "\t" + str(v) + "\n")
	outputTreeFile.write("\n")	
	

	outputTreeFile.write("NODE\tNODE_FREQS\tCLADE_FREQS\n")
	for v in range(treeSize):
		outputTreeFile.write(str(v) + "\t" + ",".join([floatToStr(x[v,s].X + epsilon) for s in sampleIDs]) + "\t")
		outputTreeFile.write(",".join([floatToStr(y[v,s].X + epsilon) for s in sampleIDs]) + "\n")
	outputTreeFile.write("\n")


	outputTreeFile.write("MUT_ID\tOBSERVED_FREQS\tINFERRED_FREQS\tABS_OBSERVED_MINUS_INFERRED\n")
	for mut_id in mutIDs:
		outputTreeFile.write(mut_id + "\t")
		outputTreeFile.write(",".join([floatToStr(f[mut_id][s]) for s in sampleIDs]) + "\t")
		v = nodeOfCluster[c[mut_id]] # v is node to which mutation is assigned
		outputTreeFile.write(",".join([floatToStr(y[v,s].X) for s in sampleIDs]) + "\t")
		outputTreeFile.write(",".join([floatToStr(abs(f[mut_id][s] - (y[v, s].X))) for s in sampleIDs]) + "\n")

	outputTreeFile.close()

treeScoresFile.close()

